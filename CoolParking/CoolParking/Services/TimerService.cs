﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.VisualBasic;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using Timer = System.Threading.Timer;

namespace CoolParking.BL.Services
{
    public class TimeService : ITimerService
    {
        private static System.Timers.Timer aTimer;
        private static System.Timers.Timer aTimerTransaction;
        private static double interval = 5000;
        LogService _logService;
        public double Interval
        {
            set
            {
                interval = value;
            }
            get { return interval; }
        }
        public TimeService()
        {
            _logService = new LogService("C:/Users/Maxisoft/source/repos/CoolParking/CoolParking/bin/Debug/log.txt");
        }


        public event ElapsedEventHandler Elapsed;
        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
        public void Start()
        {
            SetTimer();
            Console.WriteLine("Parking has started working");
        }
        private static void SetTimer()
        {
            aTimer = new System.Timers.Timer(5000);
            aTimerTransaction = new System.Timers.Timer(Settings.LogFile);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimerTransaction.Elapsed += OnTimeEventTransaction;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            aTimerTransaction.AutoReset = true;
            aTimerTransaction.Enabled = true;
        }
        private static void OnTimeEventTransaction(Object source, ElapsedEventArgs e)
        {
            LogService _logService = new LogService("C:/Users/Maxisoft/source/repos/CoolParking/CoolParking/bin/Debug/log.txt");
            _logService.Read();
        }
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            TransactionInfo transactionInfo = new TransactionInfo();
            LogService _logService = new LogService("C:/Users/Maxisoft/source/repos/CoolParking/CoolParking/bin/Debug/log.txt");
            foreach (Vehicle v in Parking.GetInstance().Vehicles)
            {
                if (v.VehicleType == VehicleType.Bus)
                {
                    if (v.Balance < 0)
                    {
                        v.Balance += (decimal)Settings.Bus * (decimal)Settings.koef;
                        transactionInfo.Sum = (decimal)Settings.Bus * (decimal)Settings.koef;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);

                    }
                    else if (v.Balance < (decimal)Settings.Bus)
                    {
                        v.Balance += -((decimal)Settings.Bus - v.Balance) * (decimal)Settings.koef;
                        transactionInfo.Sum = ((decimal)Settings.Bus - v.Balance) * (decimal)Settings.koef; ;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    else
                    {
                        v.Balance -= (int)Settings.Bus;
                        transactionInfo.Sum = (int)Settings.Bus;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    _logService.Write($"{transactionInfo.Vehicle.Id}, {transactionInfo.TransactionTime}, {transactionInfo.Sum}");
                    Parking.GetInstance().ParkingBalance += (int)Settings.Bus;
                    Parking.GetInstance().EarnedMoney += (int)Settings.Bus;
                    transactionInfo.Sum = (int)Settings.Bus;
                    transactionInfo.TransactionTime = DateAndTime.Now;
                    transactionInfo.Vehicle = v;
                    Parking.GetInstance().Transactions.Add(transactionInfo);

                }
                else if (v.VehicleType == VehicleType.Motorcycle)
                {
                    if (v.Balance < 0)
                    {
                        v.Balance += Settings.Motorcycle * (decimal)Settings.koef;
                        transactionInfo.Sum = Settings.Motorcycle * (decimal)Settings.koef;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    else if (v.Balance < Settings.Motorcycle)
                    {
                        v.Balance += -(Settings.Motorcycle - v.Balance) * (decimal)Settings.koef;
                        transactionInfo.Sum = (Settings.Motorcycle - v.Balance) * (decimal)Settings.koef;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    else
                    {
                        v.Balance -= Settings.Motorcycle;
                        transactionInfo.Sum = Settings.Motorcycle;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    Parking.GetInstance().ParkingBalance += (int)Settings.Motorcycle;
                    Parking.GetInstance().EarnedMoney += (int)Settings.Motorcycle;
                    _logService.Write($"{transactionInfo.Vehicle.Id}, {transactionInfo.TransactionTime}, {transactionInfo.Sum}");
                    transactionInfo.Sum = (int)Settings.Motorcycle;
                    transactionInfo.TransactionTime = DateAndTime.Now;
                    transactionInfo.Vehicle = v;
                    Parking.GetInstance().Transactions.Add(transactionInfo);
                }
                if (v.VehicleType == VehicleType.PassengerCar)
                {
                    if (v.Balance < 0)
                    {
                        v.Balance += Settings.PassengerCar * (decimal)Settings.koef;
                        transactionInfo.Sum = Settings.PassengerCar * (decimal)Settings.koef;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    else if (v.Balance < Settings.PassengerCar)
                    {
                        v.Balance += -(Settings.PassengerCar - v.Balance) * (decimal)Settings.koef;
                        transactionInfo.Sum = (Settings.PassengerCar - v.Balance) * (decimal)Settings.koef;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    else
                    {
                        v.Balance -= Settings.PassengerCar;
                        transactionInfo.Sum = Settings.PassengerCar;
                        transactionInfo.TransactionTime = DateAndTime.Now;
                        transactionInfo.Vehicle = v;
                        Parking.GetInstance().Transactions.Add(transactionInfo);
                    }
                    Parking.GetInstance().ParkingBalance += (int)Settings.PassengerCar;
                    Parking.GetInstance().EarnedMoney += (int)Settings.PassengerCar;
                    _logService.Write($"ParkingBalance earned now: {Parking.GetInstance().EarnedMoney += (int)Settings.PassengerCar}");
                    transactionInfo.Sum = (int)Settings.PassengerCar;
                    transactionInfo.TransactionTime = DateAndTime.Now;
                    transactionInfo.Vehicle = v;
                    Parking.GetInstance().Transactions.Add(transactionInfo);
                }
                if (v.VehicleType == VehicleType.Truck)
                {
                    if (v.Balance < 0)
                    {
                        v.Balance += Settings.Truck * (decimal)Settings.koef;
                    }
                    else if (v.Balance < Settings.Truck)
                    {
                        v.Balance += -(Settings.Truck - v.Balance) * (decimal)Settings.koef;
                    }
                    else
                    {
                        v.Balance -= Settings.Truck;
                    }
                    Parking.GetInstance().ParkingBalance += (int)Settings.Truck;
                    Parking.GetInstance().EarnedMoney += (int)Settings.Truck;
                    _logService.Write($"ParkingBalance earned now: {Parking.GetInstance().EarnedMoney += (int)Settings.Truck}");
                    transactionInfo.Sum = (int)Settings.Truck;
                    transactionInfo.TransactionTime = DateAndTime.Now;
                    transactionInfo.Vehicle = v;
                    Parking.GetInstance().Transactions.Add(transactionInfo);
                }
            }
        }
        public void Stop()
        {
            Console.WriteLine("\nParking has stoped working\n");
        }
    }
}
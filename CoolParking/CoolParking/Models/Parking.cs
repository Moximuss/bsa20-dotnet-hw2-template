﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using CoolParking.BL.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        List<Vehicle> vehicles;
        TransactionInfo[] transactions;
        ArrayList list;

        private static readonly Lazy<Parking> lazy =
        new Lazy<Parking>(() => new Parking());

        public ArrayList Transactions {
            set
            {
                list = new ArrayList(Settings.LogFile);
            }
            get { return list; }
        }


        private Parking()
        {
            Vehicles = new List<Vehicle>(Settings.Instance.ParkingSpace);
            ParkingBalance = Settings.StartBalacne;
            TransactionInfo transactionInfo = new TransactionInfo();
            TransactionInfo transactionInfo2 = new TransactionInfo();
            transactions = new TransactionInfo[Settings.LogFile];
            list = new ArrayList(Settings.LogFile);
            list.Add(transactionInfo);
            Transactions = new ArrayList(transactions);
            Transactions.Add(transactionInfo);
            Transactions.Add(transactionInfo2);
        }
        public List<Vehicle> Vehicles
        {
            set
            {
                vehicles = new List<Vehicle>(Settings.Instance.ParkingSpace);
            }
            get { return vehicles; }
        }
        public int ParkingBalance { get; set; }
        public int EarnedMoney { get; set; }
        public static Parking GetInstance()
        {
            return lazy.Value;
        }
       
    }
}
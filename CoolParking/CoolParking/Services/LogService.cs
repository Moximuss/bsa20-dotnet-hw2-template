﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Reflection.Metadata.Ecma335;

namespace CoolParking.BL
{
    public class LogService : ILogService
    {
        private string _logFilePath;
        public StreamReader fin;
        public StreamWriter fout;
        public LogService(string logFilePath)
        {
            this._logFilePath = logFilePath;
        }
        public LogService()
        {
            fin = new StreamReader(_logFilePath);
            fout = new StreamWriter(_logFilePath);
        }
        public string LogPath
        {
            get
            {
                return _logFilePath;
            }
        }
        public string Read()
        {
            using (StreamReader fin = new StreamReader(_logFilePath))
            {
                return fin.ReadToEnd();
            }
        }
        public void Write(string logInfo)
        {
            using (StreamWriter fout = new StreamWriter(_logFilePath))
            {
                fout.WriteLine(logInfo);
                fout.Close();
            }
        }
    }
}
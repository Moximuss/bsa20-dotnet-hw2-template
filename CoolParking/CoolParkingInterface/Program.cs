﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParkingInterface
{
    class Program
    {
        public Parking Parking { get; set; }
        private static void ShowMainMenu()
        {
            Console.Write("\nМеню:\n1) Вывести на экран текущий баланс Парковки\n" +
                "2) Вывести на экран сумму заработанных денег за текущий период (до записи в лог)\n" +
                "3) Вывести на экран количество свободных мест на парковке\n" +
                "4) Вывести на экран количество занятых мест на парковке\n" +
                "5) Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).\n" +
                "6) Вывести историю транзакций (считав данные из файла Transactions.log).\n" +
                "7) Вывести на экран список Тр. средств находящихся на Паркинге.\n" +
                "8) Поставить Тр. средство на Паркинг\n" +
                "9) Забрать транспортное средство с Паркинга\n" +
                "10) Пополнить баланс конкретного Тр. средства\n" +
                "0) Выход из программы\n Для потверждения команды введите нужную цифру: ");
        }
        static void Main(string[] args)
        {
           
            TimeService logTimer = new TimeService();
            LogService logService = new LogService("C:/Users/Maxisoft/source/repos/CoolParking/CoolParking/bin/Debug/log.txt");
            ParkingService parking = new ParkingService();
            VehicleType type;
            ShowMainMenu();
            while (true) {
                string key = Console.ReadLine();
                switch (key)
                {
                    case "1":
                        {
                            Console.WriteLine(Parking.GetInstance().ParkingBalance);
                            break;
                        }
                    case "2":
                        {
                            Console.WriteLine(Parking.GetInstance().EarnedMoney);
                            break;
                        }
                    case "3":
                        {
                            Console.WriteLine(parking.GetFreePlaces());
                            break;
                        }
                    case "4":
                        {
                            Console.WriteLine($"There are {parking.GetTookPlaces()} in the parking");
                            break;
                        }
                    case "5":
                        {
                            TransactionInfo[] info = parking.GetLastParkingTransactions();
                            TransactionInfo transaction = info.Last();
                            Console.WriteLine($"Last transaction is {transaction.Sum},{transaction.Vehicle.Id},{transaction.TransactionTime}");
                            break;
                        }
                    case "6":
                        {
                            Console.WriteLine(logService.Read());
                            break;
                        }
                    case "7":
                        {
                            if (parking.GetVehicles().Count > 0)
                            {
                                Console.WriteLine("All vehicles in the parking:");
                                foreach (Vehicle vehicle in parking.GetVehicles())
                                {
                                    Console.WriteLine($"{vehicle.Id} - {vehicle.VehicleType}, ");
                                }
                            } else {
                                Console.WriteLine("There noone vehicle in the parking");
                            }
                            break;
                        }
                    case "8":
                        {
                            type = new VehicleType();
                            Type t = typeof(decimal);
                            Type t2 = typeof(string);
                            Console.WriteLine("Enter a vehicle's id");
                            string vehicleId = Console.ReadLine();
                            if (vehicleId.GetType() == t2)
                            {
                                Console.WriteLine("Enter a vehicle's balacne");
                                decimal balacne = Convert.ToDecimal(Console.ReadLine());
                                if (balacne.GetType() == t)
                                {
                                    Console.WriteLine("Enter a vehicle's type");
                                    string vType = Console.ReadLine();
                                    if (vType == "Bus")
                                    {
                                        type = VehicleType.Bus;
                                    }
                                    else if (vType == "Motorcycle")
                                    {
                                        type = VehicleType.Motorcycle;
                                    }
                                    else if (vType == "PassengerCar")
                                    {
                                        type = VehicleType.PassengerCar;
                                    }
                                    else if (vType == "Truck")
                                    {
                                        type = VehicleType.Truck;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Wrong type of truck, try to enter another type");
                                    }
                                    Vehicle vehicle = new Vehicle(vehicleId, type, balacne);
                                    if (balacne.GetType() == t && vehicleId.GetType() == t2)
                                    {
                                        parking.AddVehicle(vehicle);
                                    }

                                }
                            }
                            break;
                        }
                    case "9":
                        {
                            Console.WriteLine("Enter a vehicle's id");
                            string vehicleId = Console.ReadLine();
                            parking.RemoveVehicle(vehicleId);
                            Console.WriteLine($"{vehicleId} has removed");
                            break;
                        }
                    case "10":
                        {
                            Console.WriteLine("Enter a vehicle's id");
                            string vehicleId =  Console.ReadLine();
                            decimal balacne = Convert.ToDecimal(Console.ReadLine());
                            parking.RemoveVehicle(vehicleId);
                            Console.WriteLine($"{vehicleId} has removed");
                            break;
                        }
                    case "0":
                        {
                            break;
                        }
                }
            }
        }
    }
}

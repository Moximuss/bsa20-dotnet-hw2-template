﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    class Settings
    {
        /// Property for get singleton object
        private static readonly Lazy<Settings> settings = new Lazy<Settings>(() => new Settings());
        public static Settings Instance { get { return settings.Value; } }
        public static int StartBalacne = 0;
        private static int parkingSpace = 11;
        private int Interval = 5;
        public static int LogFile = 60;
        public static double koef = 2.5;

        public static int Truck = 5;
        public static double Bus = 3.5;
        public static int PassengerCar = 2;
        public static int Motorcycle = 1;

      
        public int ParkingSpace
        {
            get { return parkingSpace; }
            set
            {
                if (value > 0)
                    parkingSpace = value;
                else
                    throw new Exception("Количество парковочных мест должно быть ненулевым!");
            }
        }
    }
}
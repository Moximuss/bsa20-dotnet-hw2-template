﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public readonly ParkingService _parkingService;
        public readonly TimeService _logTimer;
        public readonly LogService _logService;
        public ParkingService()
        {
            _logTimer = new TimeService();
            _logTimer.Start();
            _logService = new LogService("C:/Users/Maxisoft/source/repos/CoolParking/CoolParking/bin/Debug/log.txt");
        }
        public void StopWorking()
        {
            _logTimer.Stop();
            Console.WriteLine("Parking has stoped working");
        }
        public void AddVehicle(Vehicle vehicle)
        {
            Regex regex = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            MatchCollection matches = regex.Matches(vehicle.Id);
            if (matches.Count > 0)
            {
                Parking.GetInstance().Vehicles.Add(vehicle);
                Console.WriteLine($"{vehicle.Id} has added");
            } else {
                Console.WriteLine($"{vehicle.VehicleType} wasn't added, wrong id ,format AV-1234-VA, try again");
            }
        }
        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public decimal GetBalance()
        {
            return Parking.GetInstance().Vehicles.Capacity - Parking.GetInstance().Vehicles.Count;
        }

        public int GetCapacity()
        {
            return Parking.GetInstance().Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.GetInstance().Vehicles.Capacity - Parking.GetInstance().Vehicles.Count;
        }
        public int GetTookPlaces()
        {
            return Parking.GetInstance().Vehicles.Count;
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] transactions = new TransactionInfo[Parking.GetInstance().Transactions.Count];
            return transactions;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var readOnlyList = new ReadOnlyCollection<Vehicle>(Parking.GetInstance().Vehicles);
                return readOnlyList;
             
        }
        public string ReadFromLog()
        {
            return _logService.Read();
        }
        
        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = Parking.GetInstance().Vehicles.Where(c => c.Id == vehicleId).ToList().First();
            if (vehicle.Balance > 0)
                {
                Parking.GetInstance().Vehicles.Remove(vehicle);
                } else {
                    throw new InvalidOperationException("Excuse me, you must pay your debt before leaving our parking");
                }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = Parking.GetInstance().Vehicles.Where(c => c.Id == vehicleId).ToList().First();
            vehicle.Balance += sum;
            Console.WriteLine($"The balacne of{vehicle.Id} was topped up on {sum} of money\n " +
                $"Now it has {vehicle.Balance}");
        }
    }
}